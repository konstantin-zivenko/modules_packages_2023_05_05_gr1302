s = "Modules and packages"
a = [200, 300, 400, 500]
_one = "one"


def func(arg):
    print(f"arg = {arg}")


class Foo:
    pass

# print(f"in example_1: {__name__}")
if __name__ == "__main__":
    print(s)
    print(a)
    x = Foo()
    print(x)
